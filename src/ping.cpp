#include <string>

#include <SQLiteCpp/SQLiteCpp.h>

#include <deadmanswitch/ping.h>
#include <deadmanswitch/login.h>
#include <deadmanswitch/logger.h>
#include <deadmanswitch/variables.h>
#include <deadmanswitch/http.h>

DeadManSwitch::Ping::Ping(SQLite::Database* db, Login* login) {
    this->db = db;
    this->login = login;
}

bool DeadManSwitch::Ping::exec() {
    char* env = getenv("HTTP_HOST");
    std::string host(env ? env : "");
    bool was_online;
    int host_id, ping_id;

    if (host.empty()) {
        sendHttpResponse(404, "", "Host not provided");
        return false;
    }

    // Get host_id
    SQLite::Statement q_slct(*this->db, "SELECT \"hosts\".\"id\", COALESCE(\"hosts\".\"online\", false) FROM \"hosts\" WHERE \"hosts\".\"uuid\" = ? AND \"hosts\".\"active\" = true");
    q_slct.bind(1, host);
    if(!q_slct.executeStep()) {
        sendHttpResponse(404, "", "Host not found");
        return false;
    }

    host_id = q_slct.getColumn(0).getInt();
    was_online = q_slct.getColumn(1).getInt() == 1;

    SQLite::Statement q_ping(*this->db, "INSERT INTO \"pings\" (\"host_id\") VALUES (?)");
    q_ping.bind(1, host_id);
    q_ping.exec();
    ping_id = this->db->getLastInsertRowid();

    this->login->setAction(ping_id);

    if(was_online) {
        SQLite::Statement q_updt(*this->db, "UPDATE \"hosts\" SET \"last_ping_id\" = ? WHERE \"id\" = ?");
        q_updt.bind(1, ping_id);
        q_updt.bind(2, host_id);
        q_updt.exec();
    } else {
        SQLite::Statement q_updt(*this->db, "UPDATE \"hosts\" SET \"last_ping_id\" = ?, \"online_ping_id\" = ?, \"online\" = 1 WHERE \"id\" = ?");
        q_updt.bind(1, ping_id);
        q_updt.bind(2, ping_id);
        q_updt.bind(3, host_id);
        q_updt.exec();
    }
    sendHttpResponse(201, "", "Ping created");
    return true;
}
