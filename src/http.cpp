#include <string>
#include <iostream>
#include <deadmanswitch/logger.h>
#include <deadmanswitch/http.h>

void DeadManSwitch::sendHttpResponse(const int status, const std::string headers, const std::string body) {
    int log_level = DeadManSwitch::Logger::INFO;
    std::cout << "Status: " << status << std::endl;
    std::cout << "Content-Type: text/plain; charset=US-ASCII" << std::endl;
    std::cout << headers << std::endl;
    std::cout << body << std::endl;
    if (status > 399) {
        log_level = DeadManSwitch::Logger::ERROR;
    }

    char* env = getenv("REQUEST_URI");
    std::string uri(env ? env : "-");
    env = getenv("REQUEST_METHOD");
    std::string method(env ? env : "-");

    DeadManSwitch::Logger::log(log_level, method + " " + uri + " " + std::to_string(status));
}
