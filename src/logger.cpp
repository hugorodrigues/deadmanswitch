#include <string>
#include <iostream>
#include <time.h>
#include <deadmanswitch/logger.h>
#include <deadmanswitch/variables.h>

void DeadManSwitch::Logger::log(const int level, const std::string message) {
    if (level < LOG_LEVEL) {
        return;
    }
    time_t t;
    char out_t[200];
    struct tm* tmp;
    char* env = getenv("REMOTE_ADDR");
    std::string remote_addr(env ? env : "-");

    t = time(NULL);
    tmp = localtime(&t);

    if(tmp == NULL) { // Unable to log, quitting cowardly
        perror("Unable to set localtime");
    } else if(strftime(out_t, sizeof(out_t), LOG_T_FORMAT, tmp) == 0) {
        perror("Unable to set time format");
    }

    std::string level_str;
    switch(level) {
        case DEBUG:
            level_str = "DEBUG";
            break;
        case INFO:
            level_str = "INFO";
            break;
        case WARNING:
            level_str = "WARNING";
            break;
        case ERROR:
             level_str = "ERROR";
             break;
        case CRITICAL:
             level_str = "CRITICAL";
             break;
        default:
             level_str = "UNKNOWN";
    }

    std::clog << std::string(out_t) + " " + level_str + " " + remote_addr + " : " + message << std::endl;
}

void DeadManSwitch::Logger::debug(const std::string message) {
    log(DEBUG, message);
}

void DeadManSwitch::Logger::info(const std::string message) {
    log(INFO, message);
}

void DeadManSwitch::Logger::warning(const std::string message) {
    log(WARNING, message);
}

void DeadManSwitch::Logger::error(const std::string message) {
    log(ERROR, message);
}

void DeadManSwitch::Logger::critical(const std::string message) {
    log(CRITICAL, message);
}
