#include <string>
#include <SQLiteCpp/SQLiteCpp.h>

#include <deadmanswitch/login.h>
#include <deadmanswitch/variables.h>
#include <deadmanswitch/logger.h>
#include <deadmanswitch/http.h>

DeadManSwitch::Login::Login(SQLite::Database* db) {
    this->db = db;
}

bool DeadManSwitch::Login::doLogin(const std::string action) {
    this->action = action;

    char* env = getenv("HTTP_KEY");
    std::string apikey(env ? env : "");
    env = getenv("REMOTE_ADDR");
    std::string remote_addr(env ? env : "0.0.0.0");

    if (apikey.empty()) {
        sendHttpResponse(401, "", "Login information not provided");
        return false;
    }

    // Do login
    SQLite::Statement q_uid(*this->db, "SELECT \"users\".\"id\" FROM \"users\" WHERE \"users\".\"key\" = ? AND \"users\".\"active\" = true");
    q_uid.bind(1, apikey);
    if(!q_uid.executeStep()) {
        sendHttpResponse(403, "", "Invalid login");
        return false;
    }

    this->user_id = q_uid.getColumn(0).getInt();


    // Create login
    SQLite::Statement q_ins(*this->db, "INSERT INTO \"logins\" (\"user_id\", \"from\", \"action\") VALUES (?, ?, ?)");
    q_ins.bind(1, this->user_id);
    q_ins.bind(2, remote_addr);
    q_ins.bind(3, this->action);
    q_ins.exec();

    this->id = this->db->getLastInsertRowid();

    // Commit and start a new transaction
    // to ensure that the login stays registred
    this->db->exec("COMMIT");
    this->db->exec("BEGIN");

    return true;
}

void DeadManSwitch::Login::setAction(const int action_id) {
    this->action_id = action_id;
    SQLite::Statement query(*this->db, "UPDATE \"logins\" SET \"action_id\" = ? WHERE \"id\" = ?");
    query.bind(1, action_id);
    query.bind(2, this->id);
    query.exec();

}
