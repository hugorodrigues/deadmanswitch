#ifndef __cplusplus
#error A C++ compiler is required!
#endif
#if __cplusplus < 201703
#error "You must use, at least, the 2017 C++ standard"
#endif

#include <iostream>
#include <fstream>
#include <string>

#include <sqlite3.h>
#include <SQLiteCpp/SQLiteCpp.h>

#include <deadmanswitch/variables.h>
#include <deadmanswitch/logger.h>
#include <deadmanswitch/login.h>
#include <deadmanswitch/ping.h>
#include <deadmanswitch/cron.h>
#include <deadmanswitch/http.h>

using namespace DeadManSwitch;

int main() {
    // Set UTC TZ before everything else
    putenv((char *)"TZ=UTC");
    putenv((char *)"LC_TIME=UTC");

    int exit_status = EXIT_FAILURE;

    // Set log output

    std::string logfile(LOG_OUTPUT);
    std::streambuf *clogbuf = std::clog.rdbuf();
    std::ofstream log_stream;
    if (!logfile.empty()) {

        log_stream.open(LOG_OUTPUT);
        std::clog.rdbuf(log_stream.rdbuf());
    }

    char* env = getenv("REQUEST_METHOD");
    std::string request_method(env ? env : "");
    env = getenv("REQUEST_URI");
    std::string path(env ? env : "");

    if (path.empty()) { // Ensure that we are a CGI
        std::cout << "DeadManSwitch " << __DMS_VERSION__ << std::endl;
        std::cout << "Compiled at " << __DATE__ << " " << __TIME__;
#ifdef __clang__
        std::cout << " using CLang " << __clang_version__;
#elif __GNUC__
        std::cout << " using GCC " << __VERSION__;
#endif
        std::cout << " with C++ standard " << __cplusplus << std::endl;
        std::cout << "Using:" << std::endl;
        std::cout << "    * SQLiteCpp " << SQLITECPP_VERSION " with SQLite " << SQLITE_VERSION << std::endl;
        std::cout << std::endl << "If you are seeing this it's because you aren't running as CGI" << std::endl;
    } else if (request_method.empty() || request_method != "PUT") { // Ensure that request method is set and it's PUT
        if (path == "/") {
            sendHttpResponse(200, "", "DeadManSwitch " + std::string(__DMS_VERSION__));
        } else {
            sendHttpResponse(405, "", "Method not allowed");
        }
    } else {

        // Remove first / on path
        if(path[0] == '/') {
            path.erase(0, 1);
            DeadManSwitch::Logger::debug("Removed leading /");
        }

        try {

            bool encrypted = !SQLite::Database::isUnencrypted(DB_PATH);
            SQLite::Database db(DB_PATH, SQLite::OPEN_READWRITE);
            if (encrypted) {
#ifndef DB_KEY
                throw std::runtime_error("Database is encrypted but no key was defined");
#else
                try {
                    db.key(DB_KEY);
                } catch (std::exception const &e) {
                    throw std::runtime_error("Unable to decrypt database");
                }
#endif
            } else {
                Logger::warning("Database is not encrypted, data is stored in plaintext");
            }
            SQLite::Transaction tx(db);

            Login login (&db);
            bool status = login.doLogin(path);

            if (status) {

                DeadManSwitch::Logger::info("Calling action " + path);

                if (path == "ping") {
                    Ping ping (&db, &login);
                    status = ping.exec();
                } else if (path == "cron") {
                    Cron cron (&db, &login);
                    status = cron.exec();
                } else if (path == "") {
                    sendHttpResponse(200, "", "DeadManSwitch " + std::string(__DMS_VERSION__));
                    status = true;
                } else {
                    sendHttpResponse(404, "", "Path not found");
                    status = false;
                }

                if (status) {
                    tx.commit();
                }
                exit_status = EXIT_SUCCESS;
            }

        }
        catch (std::exception const &e)
        {
            DeadManSwitch::Logger::error(e.what());
            sendHttpResponse(500, "", "Something bad happened on our side");
        }
    }
    std::clog.rdbuf(clogbuf);
    if (!logfile.empty()) {
        log_stream.close();
    }
    exit(exit_status);
}
