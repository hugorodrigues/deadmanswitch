#include <string>

#include <SQLiteCpp/SQLiteCpp.h>
#include <nlohmann/json.hpp>
#include <HTTPRequest.hpp>

#include <deadmanswitch/cron.h>
#include <deadmanswitch/variables.h>
#include <deadmanswitch/logger.h>
#include <deadmanswitch/login.h>
#include <deadmanswitch/http.h>

DeadManSwitch::Cron::Cron(SQLite::Database* db, Login* login) {
    this->db = db;
    this->login = login;
}

bool DeadManSwitch::Cron::exec() {

    int cron_id;
    bool completed = false, cron_result = false;

    // Check if a cron is already running
    int count = this->db->execAndGet("SELECT COUNT(*) FROM \"crons\" WHERE \"ended\" IS NULL").getInt();

    if(count > 0) {
        sendHttpResponse(425, "Retry-After: 60\n", "A cron is already running");
        return false;
    }

    // Create cron
    this->db->exec("INSERT INTO \"crons\" (\"started\") VALUES (CURRENT_TIMESTAMP)");
    cron_id = this->db->getLastInsertRowid();
    this->login->setAction(cron_id);
    
    // Commit and start a new transaction
    // to ensure that the cron stays registred
    this->db->exec("COMMIT");
    this->db->exec("BEGIN");
    this->db->exec("SAVEPOINT DMS_CRON_EXECUTION");

    try {
        cron_result = this->do_run();
        completed = true;
        this->db->exec("RELEASE SAVEPOINT DMS_CRON_EXECUTION");
    } catch (std::exception const &e) {
        this->db->exec("ROLLBACK TO SAVEPOINT DMS_CRON_EXECUTION");
        DeadManSwitch::Logger::error(e.what());
        cron_result = false;
        completed = false;
    }

    SQLite::Statement q_uptd(*this->db, "UPDATE \"crons\" SET \"ended\" = CURRENT_TIMESTAMP, \"completed\" = ? WHERE \"id\" = ?");
    q_uptd.bind(1, completed);
    q_uptd.bind(2, cron_id);
    q_uptd.exec();
    
    // Commit because we don't want to lose
    // the cron status
    this->db->exec("COMMIT");
    this->db->exec("BEGIN");

    if (cron_result) {
        sendHttpResponse(200, "", "Cron executed");
    } else {
        sendHttpResponse(500, "", "Unable to execute cron");
    }
    return cron_result;
}

bool DeadManSwitch::Cron::do_run() {

    bool cron_res = true;

    nlohmann::json notifications = {
        {"offline", {}},
        {"online", {}}
    };

    SQLite::Statement q_off_updt(*this->db, "UPDATE \"hosts\" SET \"online\" = false, \"online_ping_id\" = NULL WHERE \"uuid\" = ?");
    SQLite::Statement q_off_slct(*this->db, "SELECT \"hosts\".\"name\", \"pings\".\"when\", \"hosts\".\"uuid\" FROM \"hosts\" JOIN \"pings\" ON \"pings\".\"id\" = \"hosts\".\"last_ping_id\" WHERE \"hosts\".\"online\" = true AND \"hosts\".\"active\" = true AND \"pings\".\"when\" < datetime(CURRENT_TIMESTAMP, '-90 seconds')");
    while (q_off_slct.executeStep()) {
        std::string name = q_off_slct.getColumn(0);
        std::string when = q_off_slct.getColumn(1);
        std::string uuid = q_off_slct.getColumn(2);
        Logger::debug("Host " + name + " (" + uuid + ") is offline. Last ping was at " + when);
        notifications["offline"].push_back({{"host", name}, {"last_seen", when}});
        q_off_updt.reset();
        q_off_updt.clearBindings();
        q_off_updt.bind(1, uuid);
        q_off_updt.exec();
    }

    SQLite::Statement q_on_updt(*this->db, "UPDATE \"hosts\" SET \"online_ping_id\" = NULL WHERE \"id\" = ?");
    SQLite::Statement q_on_slct(*this->db, "SELECT \"hosts\".\"id\", \"hosts\".\"uuid\", \"hosts\".\"name\", \"op\".\"when\", \"lp\".\"when\" FROM \"hosts\" JOIN \"pings\" AS \"op\"  ON \"hosts\".\"online_ping_id\" = \"op\".\"id\" JOIN \"pings\" AS \"lp\" ON \"hosts\".\"last_ping_id\" = \"lp\".\"id\" WHERE \"hosts\".\"online\" = true AND \"hosts\".\"online_ping_id\" IS NOT NULL");
    while (q_on_slct.executeStep()) {
        int id = q_on_slct.getColumn(0);
        std::string uuid = q_on_slct.getColumn(1).getString();
        std::string name = q_on_slct.getColumn(2).getString();
        std::string online_ping = q_on_slct.getColumn(3).getString();
        std::string last_ping = q_on_slct.getColumn(4);
        Logger::debug("Host " + name + " (" + uuid + ") is back online since " + online_ping +". Last ping was at " + last_ping);
        notifications["online"].push_back({{"host", name}, {"online_since", online_ping}});
        q_on_updt.reset();
        q_on_updt.clearBindings();
        q_on_updt.bind(1, id);
        q_on_updt.exec();
    }

    if (!notifications["offline"].is_null() || !notifications["online"].is_null()) {
        // Check if HTTP is enabled
        std::string http_endpoint = this->getConfig("notifications.http.endpoint", "");
        if (!http_endpoint.empty()) {
            std::vector<std::string> headers{"Content-Type: application/json"};
            SQLite::Statement q_cnf(*this->db, "SELECT \"key\", \"value\" FROM \"configs\" WHERE \"key\" LIKE 'notifications.http.headers.%'");
            while(q_cnf.executeStep()) {
                std::string rawkey = q_cnf.getColumn(0).getString();
                std::string key(rawkey.substr(rawkey.rfind(".") + 1));
                std::string value = q_cnf.getColumn(1).getString();
                if (key != "Content-Type" && key != "Host" && key != "Content-Length") {
                    headers.push_back(key + ": " + value);
                } else {
                    Logger::warning("The header " + key + " can't by overridden");
                }
            }

            Logger::debug("Sending request to " + http_endpoint);
            http::Request request(http_endpoint);
            const http::Response response = request.send("POST", notifications.dump(), headers);

            if (response.status < 300) {
                Logger::info("Notification successfully sent");
                Logger::debug(std::string(response.body.begin(), response.body.end()));
            } else {
                Logger::error("Notification request completed but endpoint reported " + std::to_string(response.status) + " " + std::string(response.body.begin(), response.body.end()));
                cron_res = false;
            }
        } else {
            Logger::debug("HTTP notifications disabled");
        }

    }

    return cron_res;
}

std::string DeadManSwitch::Cron::getConfig(const std::string key) {
    return this->getConfig(key, NULL);
}

std::string DeadManSwitch::Cron::getConfig(const std::string key, const std::string deflt) {
    std::string result = deflt;
    SQLite::Statement q_cnf(*this->db, "SELECT \"value\" FROM \"configs\" WHERE \"key\" = ?");
    q_cnf.bind(1, key);
    if(q_cnf.executeStep()) {
        result = q_cnf.getColumn(0).getString();
    }
    return result;
}
