--- SQL to create database schema

-- Table to hold users
CREATE TABLE IF NOT EXISTS "users" (
    "id" INTEGER CONSTRAINT "users_pkey" PRIMARY KEY AUTOINCREMENT NOT NULL,
    "name" VARCHAR(20) NOT NULL,
    "key" VARCHAR(100) NOT NULL CONSTRAINT "users_unique_key" UNIQUE CONSTRAINT "users_check_key" CHECK(LENGTH("key") >= 16),
    "active" BOOLEAN NOT NULL DEFAULT true
);

-- Table to record user logins
CREATE TABLE IF NOT EXISTS "logins" (
    "id" INTEGER CONSTRAINT "logins_pkey" PRIMARY KEY AUTOINCREMENT NOT NULL,
    "user_id" INTEGER NOT NULL CONSTRAINT "logins_user_id_fkey" REFERENCES "users" ("id") ON DELETE CASCADE,
    "when" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "from" VARCHAR(255) NOT NULL DEFAULT "0.0.0.0",
    "action" CHAR(4) NOT NULL,
    "action_id" INTEGER
);

-- Hosts that to monitor
CREATE TABLE IF NOT EXISTS "hosts" (
    "id" INTEGER CONSTRAINT "hosts_pkey" PRIMARY KEY AUTOINCREMENT NOT NULL,
    "uuid" VARCHAR(36) NOT NULL CONSTRAINT "hosts_unique_uuid" UNIQUE,
    "name" VARCHAR(20) NOT NULL,
    "location" VARCHAR(50),
    "active" BOOLEAN,
    "online" BOOLEAN
);

-- Hosts pings
CREATE TABLE IF NOT EXISTS "pings" (
    "id" INTEGER CONSTRAINT "pings_pkey" PRIMARY KEY AUTOINCREMENT NOT NULL,
    "host_id" INTEGER NOT NULL CONSTRAINT "pings_host_id_fkey" REFERENCES "hosts" ("id") ON DELETE CASCADE,
    "when" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP
);

-- Add ping columns to hosts
ALTER TABLE "hosts" ADD COLUMN "last_ping_id" INTEGER CONSTRAINT "hosts_last_ping_id_fkey" REFERENCES "pings" ("id") ON DELETE SET NULL;
ALTER TABLE "hosts" ADD COLUMN "online_ping_id" INTEGER CONSTRAINT "hosts_online_ping_id_fkey" REFERENCES "pings" ("id") ON DELETE SET NULL;

-- Executed crons
CREATE TABLE IF NOT EXISTS "crons" (
    "id" INTEGER CONSTRAINT "crons_pkey" PRIMARY KEY AUTOINCREMENT NOT NULL,
    "started" DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "ended" DATETIME,
    "completed" BOOLEAN
);

-- Table to configurations
CREATE TABLE IF NOT EXISTS "configs" (
    "id" INTEGER CONSTRAINT "configs_pkey" PRIMARY KEY AUTOINCREMENT NOT NULL,
    "key" VARCHAR(255) NOT NULL CONSTRAINT "configs_unique_key" UNIQUE,
    "value" VARCHAR(255) NOT NULL
);

-- Create configurations
INSERT INTO "configs" ("key", "value") VALUES ("db.version", "2021030101") ON CONFLICT DO NOTHING;
INSERT INTO "configs" ("key", "value") VALUES ("aws.accesskey", "") ON CONFLICT DO NOTHING;
INSERT INTO "configs" ("key", "value") VALUES ("aws.secretkey", "") ON CONFLICT DO NOTHING;
INSERT INTO "configs" ("key", "value") VALUES ("aws.region", "") ON CONFLICT DO NOTHING;
INSERT INTO "configs" ("key", "value") VALUES ("aws.snsarn", "") ON CONFLICT DO NOTHING;

-- End if version 2021030101
-- Remove AWS configurations. Not needed anymore
DELETE FROM "configs" WHERE "key" LIKE "aws.%";
INSERT INTO "configs" ("key", "value") VALUES ("notifications.http.endpoint", "") ON CONFLICT DO NOTHING;

UPDATE "configs" SET "value"="2021030301" WHERE "key" = "db.version";

SELECT 'Current DB version ' || "value" FROM "configs" WHERE "key" = "db.version";
