# DeadManSwitch
-------
DeadManSwitch is a C++ CGI program to send alerts when a host stops sending keepalives.

# About DeadManSwitch
This is a statically compiled C++ program that can be used on any host provider that allows CGI programs.

It uses a SQLite database (that can be encrypted due to [sqleet](https://github.com/resilar/sqleet)) to track the status of each host.

It uses a HTTP POST request to send notifications, allowing for custom notification system.

# License
DeadManSwitch has been released in the public domain under the [UNLICENSE](https://unlicense.org/) license.

# Goals of DeadManSwitch
* Able to be used on free hosting providers that support CGI.
* Be statically compiled to not depend on provider libraries.
* Be small and fast as possible, with fastest being the priority.

# Intended use
DeadManSwitch isn't intended to replace any monitorization service, but instead to complement it (although if you only want to know if your hosts are down, this is good enough).

The initial idea was to notify if my homelab prometheus was down (or unable to send notifications because internet was down).

# How to use
0. [Configure](#configure) for your needs;
1. [Compile](#compile) it;
2. [Create and populate](#database) the database;
3. Deploy it as a CGI app;
4. Set your hosts to send [keepalive](#keepalive) requests
5. Setup [Cron](#cron)

## Configure
DeadManSwitch can be configured by creating the header file include/deadmanswitch/config.h

#### Options


|Option      |Valid options                                                                 |Description               |Default               |
|------------|------------------------------------------------------------------------------|--------------------------|----------------------|
|DB_PATH     |Any valid path                                                                |Path to SQLite database   |./deadmanswitch.db    |
|DB_KEY      |Any string                                                                    |Key for encrypted database|None                  |
|LOG_OUTPUT  |Any valid path                                                                |Path to log file          |None                  |
|LOG_T_FORMAT|A valid [strftime](https://www.cplusplus.com/reference/ctime/strftime/) format|Time format for logging   |%Y-%m-%d %H:%M:%S (%Z)|
|LOG_LEVEL   |LOG_DEBUG, LOG_INFO, LOG_WARNING, LOG_ERROR, LOG_CRITICAL                     |Minimum level to show logs|LOG_INFO              |

If you are familiar with Python logging, LOG_LEVEL is similar to *level* in  logging.Logger.setLevel

Example

    #define DB_PATH "/var/db/deadmanswitch.db"
    #define LOG_OUTPUT "/var/log/deadmanswitch.log"
    #define LOG_T_FORMAT "%d-%m-%Y %H:%M"
    #define LOG_LEVEL LOG_ERROR

## Compile

From your system, DeadManSwitch needs:

* cmake (to build SQLiteCpp)
* static libraries for stdc++ and libc
* make
* patch
* git
* C and C++ compiler (tested with GCC and Cmake)
* find

The rest of the dependencies are bundled with the repository.

### Install dependencies

#### RedHat/RockyLinux/CentOS
    # dnf install -y dnf-plugins-core
    # dnf config-manager --set-enabled powertools
    # dnf install -y cmake make patch git glibc-static libstdc++-static gcc gcc-c++ findutils

#### Fedora
    # dnf install -y cmake make patch git glibc-static libstdc++-static gcc gcc-c++ findutils coreutils

#### Debian
    # apt install -y cmake make patch git findutils gcc g++ coreutils

#### Alpine Linux
    # apk add cmake make patch git findutils coreutils gcc g++

### Clone and compile
    git clone --recurse-submodules https://gitlab.com/hmrodrigues/deadmanswitch.git
    cd deadmanswitch
    make

Note: This commands were tested on Docker images registry.fedoraproject.org/fedora:33, registry.centos.org/centos:8, docker.io/library/debian:10.8-slim and docker.io/library/alpine:3.12.4

On my humble machine (Fedora 33 on Intel(R) Pentium(R) CPU  N3700, 8G of RAM and a SSD) it takes about 45 seconds to compile everything

## Database
### Create
Create a SQLite database and read the file database.sql to create the initial tables.

    sqlite3 deadmanswitch.db
    .read database.sql

If you want an encrypted database (highly recommened if using a shared hosting provider), you can compile SQLite3 using the bundled sqleet

    make local/bin/sqlite3

and run:

    local/bin/sqlite3 deadmanswitch.db
    PRAGMA rekey "your very secure passphrase"
    .quit
    local/bin/sqlite3 deadmanswitch.db
    PRAGMA key "your very secure passphrase"
    .read database.sql

Don't forget to set DB_KEY

### Populate
There are three table that you need to populate in order to start using DeadManSwitch

Only the columns that you need to set are shown.

**Bold** columns must have unique records

#### Hosts
This table holds the hosts that you want to monitor

|Column        |Description                                 |Default|Required|
|--------------|--------------------------------------------|-------|--------|
|**uuid**      |Host unique identifier, used to create pings|None   |Yes     |
|name          |Human friendly name of the host             |None   |Yes     |
|location      |Host location                               |None   |No      |
|active        |Only process host if this column is true    |true   |Yes     |

New host example: `INSERT INTO "hosts" ("uuid", "name", "location", "active") VALUES ("de808aae-a3cb-4ef8-b0f8-3d9a4b28d2aa", "localhost", "Home", true);`

Update host example: `UPDATE "hosts" SET "name"="new name" WHERE "uuid"="de808aae-a3cb-4ef8-b0f8-3d9a4b28d2aa";`

#### Users
Holds the users to authenticate the requests

|Column |Description                           |Default|Required|
|-------|--------------------------------------|-------|--------|
|name   |Human friendly name of the user       |None   |Yes     |
|**key**|Key used by the user to authenticate  |None   |Yes     |
|active |Only process request if user is active|true   |Yes     |

New user example: `INSERT INTO "users" ("name", "key", "active") VALUES ("user1", "this is a secure key", true);`

Update user example: `UPDATE "users" SET "active"=false WHERE "name"="user1";`

#### Configurations
Holds DeadManSwitch runtime configurations

|Column |Description        |Default|Required|
|-------|-------------------|-------|--------|
|**key**|Configuration key  |None   |Yes     |
|value  |Configuration value|None   |Yes     |

New configuration example: `INSERT INTO "configs" ("key", "value") VALUES ("config.key", "config value");`

Update configuration example: `UPDATE "configs" SET "value"="new config value" WHERE "key"="config.key";`

Available configurations:

|Key                                  |Description                                                             |
|-------------------------------------|------------------------------------------------------------------------|
|db.version                           |Current database version (to be used by future migration feature)       |
|notifications.http.endpoint          |Endpoint for HTTP notifications                                         |
|notifications.http.headers.`<header>`|Custom headers for HTTP notification. `<header>` is the header to be set|


### Side note
If you are using a public shared provider, I really recommend you to encrypt your database. Check how on the [create](#create) step.

## Keepalive
After setting up DeadManSwitch you need to configure your hosts. There isn't a client available, but any piece of software that can make HTTP(S) requests this custom headers will work. Curl example:

`curl -X PUT -H "KEY: this is a secure key" -H "HOST: de808aae-a3cb-4ef8-b0f8-3d9a4b28d2aa" https://deadmanswitch.example.com/ping`

## Cron
Since this software was intended to be used as CGI, we need to call it from time to time to check the hosts. The cron must be called via HTTP request on path /cron. Why is that? Well, most free hosting providers only allow 2 cron executions per day, and that isn't enough to have a real monitoring system.

You can use one of the following free services run cron (and also check if DeadManSwitch is working):

|Service                                                                                   |Check Interval|
|------------------------------------------------------------------------------------------|--------------|
|[Nixstats](https://nixstats.com/) (EU based)                                              |1 minute      |
|[HetrixTools](https://hetrixtools.com/uptime-monitor/)                                    |1 minute      |
|[How Fast](https://www.howfast.tech)                                                      |1 minute      |
|[freshping](https://www.freshworks.com/website-monitoring/)                               |1 minute      |
|[Better Uptime](https://betteruptime.com)                                                 |3 minutes     |
|[UpTimeRobot](https://uptimerobot.com/)                                                   |5 minutes     |
|And many [more](https://alternativeto.net/software/nixstats/?license=free&platform=online)|              |

Curl example: `curl -X PUT -H "KEY: this is a secure key" https://deadmanswitch.example.com/cron`

# Dependencies

DeadManSwitch is built with the following awesome projects, kudos to them.

* [SQLiteCpp](http://srombauts.github.io/SQLiteCpp/) by [Sébastien Rombauts](https://github.com/SRombauts). MIT License
* [sqleet](https://github.com/resilar/sqleet) by [resilar](https://github.com/resilar). Public domain
* [json](https://json.nlohmann.me/) by [Niels Lohmann](http://nlohmann.me/). MIT License
* [HTTPRequest](https://github.com/elnormous/HTTPRequest) by [Elviss Strazdins](https://github.com/elnormous). Public domain

# How to contribute

## FSFE supporter
If you are a FSFE supporter (you are awesome by the way), just fork and pull-request your changes

## Non FSFE supporter
Send me your patch via email. Be sure to use the git email format (`git show --pretty=email`). You can also become a FSFE supporter (just saying)

In any case, please sign the WAIVER file

```
echo -e "## your email\n" >> AUTHORS
gpg --detach-sign --sign --armor --no-version -o - WAIVER >> AUTHORS 
```

And send me the public GPG key for validation.
