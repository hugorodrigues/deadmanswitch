# System
ifeq ($(uname_p),i386)
	LIBDIR := lib
else
	LIBDIR := lib64
endif

# Directories
BASEDIR := $(shell pwd)
SRCDIR := src
BUILDDIR := build
LOCALDIR := local
TARGET := bin/deadmanswitch

# C Macros
GIT_HASH ?= $(shell git rev-parse --short HEAD 2>/dev/null)
GIT_TAG ?= $(shell git tag --points-at HEAD 2>/dev/null)
 
# Building
SRCEXT := cpp
SOURCES := $(shell find $(SRCDIR) -type f -name *.$(SRCEXT))
OBJECTS := $(patsubst $(SRCDIR)/%,$(BUILDDIR)/%,$(SOURCES:.$(SRCEXT)=.o))
LINKFLAGS := -static
CFLAGS := -std=c++17 -Wall -Wextra -Werror
LIB := -L lib -L $(LOCALDIR)/$(LIBDIR)
LIBRARIES := -lSQLiteCpp -lsqlite3 -ldl -pthread
INC := -I include -I $(LOCALDIR)/include

ifneq ($(GIT_HASH),)
	CFLAGS += -D__GIT_HASH__='"$(GIT_HASH)"'
endif
ifneq ($(GIT_TAG),)
	CFLAGS += -D__GIT_TAG__='"$(GIT_TAG)"'
endif

ifeq ($(LIBDIR),lib64)
	LIB += -L $(LOCALDIR)/lib
endif

# SQLite
SQLITE_LIB := $(LOCALDIR)/$(LIBDIR)/libsqlite3.a
SQLITE_BIN := $(LOCALDIR)/bin/sqlite3
SQLITE_BUILDDIR := $(BUILDDIR)/sqlite

# SQLiteCpp
SQLITECPP_LIB := $(LOCALDIR)/$(LIBDIR)/libSQLiteCpp.a
SQLITECPP_BUILDDIR := $(BUILDDIR)/sqlitecpp
SQLITECPP_CMAKE_OPTS := -DCMAKE_INSTALL_PREFIX=$(BASEDIR)/$(LOCALDIR) -DCMAKE_INSTALL_OLDINCLUDEDIR=include/ -DBUILD_SHARED_LIBS="OFF" -DSQLITECPP_INTERNAL_SQLITE=OFF -DSQLite3_LIBRARY=$(BASEDIR)/$(SQLITE_LIB) -DSQLite3_INCLUDE_DIR=$(BASEDIR)/$(LOCALDIR)/include -DSQLITECPP_RUN_CPPLINT="OFF" -DSQLITECPP_RUN_CPPCHECK="OFF"

# JSON
JSON_HEADER := $(LOCALDIR)/include/nlohmann/json.hpp

# HTTP
HTTP_HEADER := $(LOCALDIR)/include/HTTPRequest.hpp

$(TARGET): $(OBJECTS)
	@echo "Linking..."
	$(CXX) $(LINKFLAGS) $^ -o $(TARGET) $(LIB) $(LIBRARIES)

$(BUILDDIR)/%.o: $(SRCDIR)/%.$(SRCEXT) $(HTTP_HEADER) $(SQLITECPP_LIB) $(JSON_HEADER)
	@mkdir -p $(BUILDDIR)
	$(CXX) $(CFLAGS) $(INC) -c -o $@ $<

$(SQLITE_LIB):
	@mkdir -p $(SQLITE_BUILDDIR) $(LOCALDIR)/$(LIBDIR) $(LOCALDIR)/include
	cd dependencies/sqleet; $(CC) -c sqleet.c -DSQLITE_TEMP_STORE=3 -DSQLITE_ENABLE_COLUMN_METADATA -o $(BASEDIR)/$(SQLITE_BUILDDIR)/sqleet.o
	ar rcs $(SQLITE_LIB) $(SQLITE_BUILDDIR)/sqleet.o
	install --mode=644 --preserve-timestamps dependencies/sqleet/sqleet.h $(LOCALDIR)/include/sqlite3.h
	install --mode=644 --preserve-timestamps dependencies/sqleet/sqlite3ext.h $(LOCALDIR)/include/sqlite3ext.h

$(SQLITE_BIN): $(SQLITE_LIB)
	@mkdir -p $(LOCALDIR)/bin
	$(CC) $(LINKFLAGS) $(INC) $(LIB) dependencies/sqleet/shell.c -o $(SQLITE_BIN) -lsqlite3 -lpthread -ldl

$(SQLITECPP_LIB): $(SQLITE_LIB)
	@mkdir -p $(SQLITECPP_BUILDDIR) $(LOCALDIR)
	cd $(SQLITECPP_BUILDDIR); cmake $(SQLITECPP_CMAKE_OPTS) $(BASEDIR)/dependencies/SQLiteCpp/
	$(MAKE) -C $(SQLITECPP_BUILDDIR)
	$(MAKE) -C $(SQLITECPP_BUILDDIR) install

$(HTTP_HEADER):
	@mkdir -p $(LOCALDIR)/include
	install --mode=644 --preserve-timestamps dependencies/HTTPRequest/include/HTTPRequest.hpp $(HTTP_HEADER)
	@patch $(HTTP_HEADER) -i dependencies/HTTPRequest.patch

$(JSON_HEADER):
	@mkdir -p $(LOCALDIR)/include
	install -d $(LOCALDIR)/include/nlohmann
	install --mode=644 dependencies/json/single_include/nlohmann/json.hpp $(JSON_HEADER)

clean:
	@echo "Cleaning...";
	$(RM) -r $(BUILDDIR) $(TARGET)

cleanall: clean
	$(RM) -r $(LOCALDIR)

.PHONY: clean
