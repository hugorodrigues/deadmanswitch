#ifndef CRON_H
#define CRON_H

#include "SQLiteCpp/SQLiteCpp.h"

#include "login.h"

namespace DeadManSwitch {

    class Cron {
        private:
            SQLite::Database* db;
            Login* login;
            bool do_run();
            std::string getConfig(const std::string, const std::string);
            std::string getConfig(const std::string);

        public:
            Cron(SQLite::Database*, Login*);
            bool exec();
    };

}

#endif
