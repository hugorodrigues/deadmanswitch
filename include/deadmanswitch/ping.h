#ifndef PING_H
#define PING_H

#include "SQLiteCpp/SQLiteCpp.h"
#include "login.h"

namespace DeadManSwitch {

    class Ping {
        private:
            SQLite::Database* db;
            Login* login;

        public:
            Ping(SQLite::Database*, Login*);
            bool exec();
    };

}

#endif
