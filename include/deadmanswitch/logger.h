#ifndef LOGGER_H
#define LOGGER_H

#include "variables.h"

namespace DeadManSwitch {

    namespace Logger {

        const int NOTSET = LOG_NOTSET;
        const int DEBUG = LOG_DEBUG;
        const int INFO = LOG_INFO;
        const int WARNING = LOG_WARNING;
        const int ERROR = LOG_ERROR;
        const int CRITICAL = LOG_CRITICAL;

        void log(const int, const std::string);
        void debug(const std::string);
        void info(const std::string);
        void warning(const std::string);
        void error(const std::string);
        void critical(const std::string);
    };

}

#endif
