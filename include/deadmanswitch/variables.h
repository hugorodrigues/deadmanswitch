#ifndef VARIABLES_H
#define VARIABLES_H

// This macros aren't configurable
#define LOG_NOTSET 0
#define LOG_DEBUG 10
#define LOG_INFO 20
#define LOG_WARNING 30
#define LOG_ERROR 40
#define LOG_CRITICAL 50

#define MAIL_HEADER "Some hosts have changed state\n"
#define MAIL_OFFLINE_HEADER "\nHosts that went offline:\nHost | Last ping | UUID\n"
#define MAIL_ONLINE_HEADER "\nHosts that are now online:\nHost | Last ping | Online on | UUID\n"
#define DEFAULT_HEADER "\n"
#define DEFAULT_OFFLINE_HEADER "\nOffline:\n"
#define DEFAULT_ONLINE_HEADER "\nOnline:\n"

#ifdef __GIT_TAG__
// Only __GIT_TAG__
#ifndef __GIT_HASH__
#define __DMS_VERSION__ __GIT_TAG__
// __GIT_TAG__ + __GIT_HASH__
#else
#define __DMS_VERSION__ __GIT_TAG__ " rev: " __GIT_HASH__
#endif
// Only __GIT_HASH__
#else
#ifdef __GIT_HASH__
#define __DMS_VERSION__ "rev: " __GIT_HASH__
#endif
#endif

#ifndef __DMS_VERSION__
#define __DMS_VERSION__ ""
#endif

// Include configurations, if they exists
#if defined(__has_include)
#if __has_include("config.h")
#include "config.h"
#endif
#endif

// Default configurations

// Database path
#ifndef DB_PATH
#define DB_PATH "deadmanswitch.db"
#endif

// Logging
#ifndef LOG_OUTPUT
#define LOG_OUTPUT ""
#endif
#ifndef LOG_T_FORMAT
#define LOG_T_FORMAT "%Y-%m-%d %H:%M:%S (%Z)"
#endif
#ifndef LOG_LEVEL
#define LOG_LEVEL LOG_INFO
#endif

#endif
