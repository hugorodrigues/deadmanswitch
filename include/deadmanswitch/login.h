#ifndef LOGIN_H
#define LOGIN_H

#include <string>

#include "SQLiteCpp/SQLiteCpp.h"

namespace DeadManSwitch {

    class Login {
        private:
            int id;
            int action_id;
            int user_id;
            std::string action;
            SQLite::Database* db;

        public:
            Login(SQLite::Database*);
            bool doLogin(const std::string);
            void setAction(const int);
    };

}

#endif
