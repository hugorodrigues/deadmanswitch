#ifndef HTTP_H
#define HTTP_H

#include <string>

namespace DeadManSwitch {

    void sendHttpResponse(const int, const std::string, const std::string);

}

#endif
