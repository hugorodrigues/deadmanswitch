Any libs that get compiled by the project, third party or any needed in development.

Prior to deployment, third party libraries get moved to /usr/local/lib where they belong

Can be used to test different library versions than the standard
